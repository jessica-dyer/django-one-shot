from django.urls import path

from todos.views import (
    TodoListDetailView,
    TodoListView,
    CreateTodoListView,
    UpdateTodoListView,
    DeleteTodoListView,
    CreateTodoItemView,
    UpdateTodoItemView,
)

urlpatterns = [
    path("", TodoListView.as_view(), name="todos_list"),
    path("<int:pk>/", TodoListDetailView.as_view(), name="todo_detail"),
    path("create/", CreateTodoListView.as_view(), name="todolist_new"),
    path("<int:pk>/edit/", UpdateTodoListView.as_view(), name="todolist_edit"),
    path(
        "<int:pk>/delete/", DeleteTodoListView.as_view(), name="todolist_delete"
    ),
    path("items/create/", CreateTodoItemView.as_view(), name="todoitem_new"),
    path(
        "items/<int:pk>/edit/",
        UpdateTodoItemView.as_view(),
        name="todoitem_edit",
    ),
]
