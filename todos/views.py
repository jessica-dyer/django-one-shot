from django.shortcuts import render, redirect
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy


from todos.models import TodoList, TodoItem


class TodoListView(ListView):
    model = TodoList
    template_name = "todos/list.html"


class TodoListDetailView(DetailView):
    model = TodoList
    template_name = "todos/detail.html"


class CreateTodoListView(CreateView):
    model = TodoList
    template_name = "todos/new.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")


class UpdateTodoListView(UpdateView):
    model = TodoList
    template_name = "todos/edit.html"
    fields = ["name"]
    success_url = reverse_lazy("todos_list")


class DeleteTodoListView(DeleteView):
    model = TodoList
    template_name = "todos/delete.html"
    success_url = reverse_lazy("todos_list")


class CreateTodoItemView(CreateView):
    model = TodoItem
    template_name = "items/new.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")


class UpdateTodoItemView(UpdateView):
    model = TodoItem
    template_name = "items/edit.html"
    fields = ["task", "due_date", "is_completed", "list"]
    success_url = reverse_lazy("todos_list")
